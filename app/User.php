<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_verified_at', 'designation_id', 'district_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static function boot(){
        parent::boot();

        if(! app()->runningInConsole()){
            static::creating(function($user){
                $user->fill([
                    'designation_id' => \App\Designation::firstOrFail()->id,
                    'district_id' => \App\District::firstOrFail()->id
                ]);
            });
        }

    }


    public function user_role(){
        return $this->hasOne(\App\UserRole::class);
    }


    public function user_attached_division(){
        return $this->hasOne(\App\UserAttachedDivision::class);
    }

    public function user_attached_district(){
        return $this->hasOne(UserAttachedDistrict::class);
    }

    public function district(){
        return $this->belongsTo(District::class);
    }

    public function designation(){
        return $this->belongsTo(Designation::class);
    }
}
