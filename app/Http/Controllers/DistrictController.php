<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use Illuminate\Http\Request;
use App\Http\Requests\DistrictStoreRequest;
use App\Http\Requests\DistrictUpdateRequest;
use Illuminate\Database\QueryException;
use TheSeer\Tokenizer\Exception;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $districts = $this->allowed_districts()->paginate(10);


        return \view('districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $divisions = Division::pluck('name', 'id');
        return \view('districts.create', compact('divisions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictStoreRequest $request)
    {
        $districts = $request->store();

        if($districts){
            return redirect('districts')->with('message', 'District Saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        \in_array($district->id, $this->allowed_districts()->pluck('id')->toArray()) ? '':abort(403);
        return \view('districts.show', compact('district'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
        \in_array($district->id, $this->allowed_districts()->pluck('id')->toArray()) ? '':abort(403);
        return \view('districts.edit', compact('district'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(DistrictUpdateRequest $request, District $district)
    {
        $districts = $request->update($district);

        if($districts){
            return redirect('districts')->with('message', 'District Updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        try{
            District::findOrFail($district->id)->delete();
        }catch(QueryException $error){
            return $error->errorInfo;
        }catch(Exception $exception){
            return $exception->getCode();
        }

        return redirect()->back()->with('message', 'District deleted!');
    }

    private function allowed_districts(){
        $districts = District::with('division')
        ->when(auth()->user()->user_role->role_id != 1, function($district){
            $district->when(auth()->user()->user_attached_district == null, function($district){
                    $district->whereHas('division', function($division){
                            $division->where('id', optional(auth()->user()->user_attached_division)->division_id);
                        });
                })
                ->orWhere('id', optional(auth()->user()->user_attached_district)->district_id)
                ;
        })
        ->orderByDesc('id');

        return $districts;
    }

    public function ajax_update(Request $request){

        if($request->ajax()){
            $district = District::findOrFail($request->id)->update([
                'name' => $request->name
            ]);
    
            if($district){
                return true;
            }
        }

    }

}
