<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Division;
use App\District;
use App\Designation;
use App\User;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Database\QueryException;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->allowed_users()->paginate(10);
        
        return \view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = config('app.roles');
        $divisions = Division::pluck('name', 'id');
        $districts = District::pluck('name', 'id');
        $designations = Designation::pluck('name', 'id');
        return \view('users.create', compact('roles', 'divisions', 'districts', 'designations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserStoreRequest $request)
    {
        $user = $request->store();

        if($user){
            return redirect('users')->with('message', 'User saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        \in_array($id, $this->allowed_users()->pluck('id')->toArray()) ? '':abort(403);
        $user = User::with('user_attached_division')->findOrFail($id);
        
        return \view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        \in_array($id, $this->allowed_users()->pluck('id')->toArray()) ? '':abort(403);
        $roles = config('app.roles');
        $divisions = Division::pluck('name', 'id');
        $districts = District::pluck('name', 'id');
        $designations = Designation::pluck('name', 'id');
        $user = User::findOrFail($id);
        return \view('users.edit', compact('roles', 'divisions', 'districts', 'designations', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = $request->update($id);

        if($user){
            return redirect('users')->with('message', 'User updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            User::findOrFail($id)->delete();
        }catch(QueryException $error){
            return $error->errorInfo;
        }catch(exception $e){
            return $e->getCode();
        }
    }

    private function allowed_users(){
        $users = User::with('user_role.role', 'designation', 'district', 'user_attached_division.division', 'user_attached_district.district')
            ->when(auth()->user()->user_role->role_id != 1, function($user){
                $user->when(auth()->user()->user_attached_district == null, function($user){
                        $user->WhereHas('district.division', function($division){
                                $division->where('id', optional(auth()->user()->user_attached_division)->division_id);
                            });
                    })
                    ->orWhere('district_id', optional(auth()->user()->user_attached_district)->district_id)
                    ;
            })
            ->orderByDesc('id');


            return $users;
    }


}
