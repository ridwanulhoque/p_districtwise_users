<?php

namespace App\Http\Controllers;

use App\Division;
use Illuminate\Http\Request;
use App\Http\Requests\DivisionStoreRequest;
use App\Http\Requests\DivisionUpdateRequest;
use PHPUnit\Framework\Exception;
use Illuminate\Database\QueryException;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisions = Division::orderByDesc('id')->get();
        return \view('divisions.index', compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \view('divisions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DivisionStoreRequest $request)
    {
        
        $divisions = $request->store();

        if($divisions){
            return redirect('divisions')->with('message', 'Division saved!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function show(Division $division)
    {
        return \view('divisions.show', compact('division'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function edit(Division $division)
    {
        return \view('divisions.edit', compact('division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function update(DivisionUpdateRequest $request, Division $division)
    {
        $divisions = $request->update($division);

        if($divisions){
            return redirect('divisions')->with('message', 'Division updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Division  $division
     * @return \Illuminate\Http\Response
     */
    public function destroy(Division $division)
    {
        try{
            Division::destroy($division->id);
        }
        catch(QueryException $error){
            return $error->errorInfo;   
        }catch(Exception $exception){
            return $exception->getCode().' You cano not delete this division';
        }

        return \redirect()->back()->with('message', 'Division deleted!');
    }

    public function ajax_update(Request $request){

        if($request->ajax()){
            $division = Division::findOrFail($request->id)->update([
                'name' => $request->name
            ]);
    
            if($division){
                return true;
            }
        }

    }

}
