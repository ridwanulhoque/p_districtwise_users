<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users',
            'designation_id' => 'required',
            'district_id' => 'required',
            'email' => 'required|email|unique:users'
        ];
    }


    public function store(){
        $user = User::create([
            'name' => $this->name,
            'email' => $this->email,
            'password' => \bcrypt('12345678'),
            'designation_id' => $this->designation_id,
            'district_id' => $this->district_id,
            'email_verified_at' => now()
        ]);

        if($this->attach_district_id > 0){
            $user->user_attached_district()->create([
                'district_id' => $this->attach_district_id
            ]);
        }

        $user->user_attached_division()->create([
            'division_id' => $this->division_id
        ]);


        return $user;
    }
}
