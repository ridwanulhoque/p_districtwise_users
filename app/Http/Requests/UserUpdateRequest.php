<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users,name,'.$this->id,
            'designation_id' => 'required',
            'district_id' => 'required',
            'email' => 'required|email|unique:users,email,'.$this->id
        ];
    }



    public function update($id){
        $user = User::findOrFail($id)->update($this->except('_token'));

        $userDb = User::findOrFail($id);
        if($this->attach_district_id > 0){
            $userDb->user_attached_district()->updateOrCreate(['user_id' => $id], [
                'district_id' => $this->attach_district_id
            ]);
        }

        $userDb->user_attached_division()->updateOrCreate(['user_id' => $id], [
            'division_id' => $this->division_id
        ]);

        $userDb->user_role()->update([
            'role_id' => $this->role_id
        ]);

        return $user;   
    }

}
