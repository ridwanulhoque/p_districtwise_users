<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\District;

class DistrictUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:districts,name,'.$this->id,
        ];
    }

    public function update($district){
        $district = District::findOrFail($district->id)->update([
            'name' => $this->name
        ]);

        return $district;
    }
}
