<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\District;

class DistrictStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:districts',
            'division_id' => 'required|not_in:0'
        ];
    }

    public function store(){
        $districts = District::create([
            'name' => $this->name,
            'division_id' => $this->division_id
        ]);

        return $districts;
    }
}
