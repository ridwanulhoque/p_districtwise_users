<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAttachedDivision extends Model
{
    protected $fillable = ['user_id', 'division_id'];


    public function division(){
        return $this->belongsTo(\App\Division::class);
    }


}
