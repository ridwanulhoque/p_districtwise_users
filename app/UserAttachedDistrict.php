<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAttachedDistrict extends Model
{
    protected $fillable = ['user_id', 'district_id'];

    public function district(){
        return $this->belongsTo(District::class);
    }
}
