<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){

    Route::get('divisions/ajax_update', 'DivisionController@ajax_update')->middleware('roles:admin');

    Route::resource('divisions', 'DivisionController')->middleware('roles:admin');

    Route::get('districts/ajax_update', 'DistrictController@ajax_update')->middleware('roles:admin,division');

    Route::resource('districts', 'DistrictController')->middleware('roles:admin,division');
    
    Route::resource('users', 'UserController')->middleware('roles:admin,division,district');   
    
});
