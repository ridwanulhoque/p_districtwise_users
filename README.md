# Instructions to run the project

	* Clone the repository git clone https://ridwanulhoque@bitbucket.org/ridwanulhoque/p_districtwise_users.git
	* Run the command composer install
	* Copy .env.example to create .env file (If .env not created)
	* Create database and modify .env file with database credentials
	* Run the command php artisan key:generate
	* Run the command php artisan migrate --seed
	* Register, login and enjoy the project