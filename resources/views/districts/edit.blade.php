@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Districts Edit') }}</div>

                <div class="card-body">
                    @if($errors->any())
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form action="{{ route('districts.update', $district->id) }}" method="post">
                        @csrf
                        @method('put')
                        <input type="hidden" name="id" value="{{ $district->id }}">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $district->name }}">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" value="Save" type="submit" class="btn btn-primary">
                        </div>            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
