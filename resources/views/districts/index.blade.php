@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Districts') }}</div>

                <div class="card-body">
                    
                    @if(Session::get('message'))
                    <div class="alert alert-success">
                            {{ Session::get('message') }}
                    </div>
                    @endif

                    <table class="table" id="districtTable">
                            <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Division</th>
                                        <th>Action</th>    
                                    </tr>   
                                </thead>
        
                                @forelse($districts as $district)
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="text" class="no-border-input" value="{{ $district->name }}" onblur="updateDistrict(this.value, {{ $district->id }})">
                                        </td>
                                        <td>{{ $district->division->name }}</td>
                                        <td>
                                            <ul class="list-unstyled list-inline">
                                                <li class="list-inline-item">
                                                    <a href="{{ route('districts.show', $district->id) }}" class="btn btn-sm btn-primary">View</a>
                                                </li>
        
                                                <li class="list-inline-item">
                                                    <a href="{{ route('districts.edit', $district->id) }}" class="btn btn-sm btn-success">Edit</a>
                                                </li>
        
                                                <li class="list-inline-item">
                                                    <a href="#" class="btn btn-sm btn-danger" onclick="formSubmit('{{ $district->id }}')">Delete</a>
        
                                                    <form action="{{route('districts.destroy', $district->id)}}" id="deleteForm_{{$district->id}}" method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
        
                                @empty
                                    <tr>
                                        <td>
                                            No data found!
                                        </td>
                                    </tr>
                                @endforelse
                    </table>  

                    {{ $districts->links() }}
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('assets/js/plugins/sweetalert.min.js')}}"></script>




<script>

    
    function formSubmit(id)
        {
            
          swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data !",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function(isConfirm) {
            if (isConfirm) {
              $('#deleteForm_'+id).submit();
              swal("Deleted!", "Your data has been deleted.", "success");
            } else {
              swal("Cancelled", "Your data is safe :)", "error");
            }
          });
        }



function updateDistrict(name, id){
    $.ajax({
        url: 'districts/ajax_update?name='+name+'&id='+id,
        type: 'GET',
        success: function(){
            alert('District updated!');
        }
    });
}
</script>
@endsection
