@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('District Show') }}</div>

                <div class="card-body">
                    
                        <table class="table table-condensed">
                                <tr>
                                    <th>District Name</th>
                                    <td>{{ $district->name }}</td>        
                                </tr>
                                <tr>
                                    <th>Division</th>
                                    <td>{{ $district->division->name ?? '' }}</td>
                                </tr>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
