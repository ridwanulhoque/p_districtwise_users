@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Division Show') }}</div>

                <div class="card-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{ $division->name }}</td>        
                            </tr>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
