@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Division List') }}</div>

                <div class="card-body">
                    
                    @if(Session::get('message'))
                    <div class="alert alert-success">
                        {{ Session::get('message') }}
                    </div>
                    @endif
                    
                    <table class="table" id="divisionTable">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Action</th>    
                            </tr>   
                        </thead>

                        @forelse($divisions as $division)
                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" class="no-border-input" value="{{ $division->name }}" onblur="updateDivision(this.value, {{ $division->id }})">
                                </td>
                                <td>
                                    <ul class="list-unstyled list-inline">
                                        <li class="list-inline-item">
                                            <a href="{{ route('divisions.show', $division->id) }}" class="btn btn-sm btn-primary">View</a>
                                        </li>

                                        <li class="list-inline-item">
                                            <a href="{{ route('divisions.edit', $division->id) }}" class="btn btn-sm btn-success">Edit</a>
                                        </li>

                                        <li class="list-inline-item">
                                            <a href="#" class="btn btn-sm btn-danger" onclick="formSubmit('{{ $division->id }}')">Delete</a>

                                            <form action="{{route('divisions.destroy', $division->id)}}" id="deleteForm_{{$division->id}}" method="POST">
                                                @csrf
                                                @method("DELETE")
                                            </form>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </tbody>

                        @empty
                            <tr>
                                <td>
                                    No data found!
                                </td>
                            </tr>
                        @endforelse
                    </table>  
                </div>
            </div>
        </div>
    </div>
</div>

{{-- <script src="{{ asset('assets/js/jquery-3.2.1.min.js') }}"></script> --}}
<!-- Data table plugin-->
<script type="text/javascript" src="{{ asset('assets/js/plugins/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/dataTables.bootstrap.min.js')}}"></script>
 
<script type="text/javascript" src="{{ asset('assets/js/plugins/sweetalert.min.js')}}"></script>


<script type="text/javascript">$('#divisionTable').DataTable();</script>


<script>

    
    function formSubmit(id)
        {
            
          swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data !",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function(isConfirm) {
            if (isConfirm) {
              $('#deleteForm_'+id).submit();
              swal("Deleted!", "Your data has been deleted.", "success");
            } else {
              swal("Cancelled", "Your data is safe :)", "error");
            }
          });
        }



function updateDivision(name, id){
    $.ajax({
        url: 'divisions/ajax_update?name='+name+'&id='+id,
        type: 'GET',
        success: function(){
            alert('Division updated!');
        }
    });
}
</script>




@endsection