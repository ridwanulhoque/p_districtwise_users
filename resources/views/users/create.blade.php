@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Users Create') }}</div>

                <div class="card-body">
                    @if($errors->any())
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form action="{{ route('users.store') }}" method="post">
                        @csrf
                       
                        <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" value="">
                        </div>

                        <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" name="email" value="">
                        </div>

                        <div class="form-group">
                                <label>User Level</label>
                                <select name="role_id_id" class="form-control">
                                    @foreach($roles as $key => $role)     
                                        <option value="{{ $key + 1 }}">{{ $role }}</option>
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                                <label>Designation</label>
                                <select name="designation_id" class="form-control">
                                    @foreach($designations as $id => $name)     
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                        </div>
                            
                        <div class="form-group">
                                <label>Division</label>
                                <select name="division_id" class="form-control">
                                    @foreach($divisions as $id => $name)     
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                        </div>

                        <div class="form-group">
                                <label>District</label>
                                <select name="attach_district_id" class="form-control">
                                    <option value="">None</option>
                                    @foreach($districts as $id => $name)     
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                        </div>
                                   
                        <div class="form-group">
                                <label>Self District</label>
                                <select name="district_id" class="form-control">
                                    @foreach($districts as $id => $name)     
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Save" type="submit" class="btn btn-primary">
                        </div>            
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
