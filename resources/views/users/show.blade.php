@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('User Show') }}</div>

                <div class="card-body">
                        <table class="table table-condensed">
                                <tr>
                                    <th>User Name</th>
                                    <td>{{ $user->name }}</td>        
                                </tr>
                                <tr>
                                    <th>User Level</th>
                                    <td>{{ $user->user_role->role->name ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>Designation</th>
                                    <td>{{ $user->designation->name ?? '' }}</td>        
                                </tr>
                                <tr>
                                    <th>Division</th>
                                    <td>{{ optional(optional($user->user_attached_division)->division)->name ?? '' }}</td>
                                </tr>
                                <tr>
                                    <th>District</th>
                                    <td>{{ optional(optional($user->user_attached_district)->district)->name ?? '' }}</td>        
                                </tr>
                                <tr>
                                    <th>Self District</th>
                                    <td>{{ $user->district->name ?? '' }}</td>
                                </tr>
                        </table>                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
