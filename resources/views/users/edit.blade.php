@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Users Edit') }}</div>

                <div class="card-body">
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <form action="{{ route('users.update', $user->id) }}" method="post">
                    @csrf
                    @method('put')

                    <input type="hidden" name="id" value="{{ $user->id  }}">
                    <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                    </div>

                    <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                    </div>

                    <div class="form-group">
                            <label>User Level</label>
                            <select name="role_id" class="form-control">
                                @foreach($roles as $key => $role)     
                                    <option value="{{ $key + 1 }}" {{ $user->user_role ? ($key+1 == $user->user_role->role_id ? 'selected' : ''):'' }}>{{ $role }}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                            <label>Designation</label>
                            <select name="designation_id" class="form-control">
                                @foreach($designations as $id => $name)     
                                    <option value="{{ $id }}" {{ $id == $user->designation_id ? 'selected': '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                    </div>
                        
                    <div class="form-group">
                            <label>Division</label>
                            <select name="division_id" class="form-control">
                                @foreach($divisions as $id => $name)     
                                    <option value="{{ $id }}" {{ $user->user_attached_division ?($id == $user->user_attached_division->division_id ? 'selected' : ''):'' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                    </div>

                    <div class="form-group">
                            <label>District</label>
                            <select name="attach_district_id" class="form-control">
                                <option value="">None</option>
                                @foreach($districts as $id => $name)     
                                    <option value="{{ $id }}" {{ $user->user_attached_district ? ($user->user_attached_district->district_id == $id ? 'selected': '') : '' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                    </div>
                               
                    <div class="form-group">
                            <label>Self District</label>
                            <select name="district_id" class="form-control">
                                @foreach($districts as $id => $name)     
                                    <option value="{{ $id }}" {{ $id == $user->district_id ? 'selected':'' }}>{{ $name }}</option>
                                @endforeach
                            </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Save" type="submit" class="btn btn-primary">
                    </div>            
                </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
