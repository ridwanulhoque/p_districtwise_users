@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Users') }}</div>

                <div class="card-body">
                    
                    @if(Session::get('message'))
                    <div class="alert alert-success">
                            {{ Session::get('message') }}
                    </div>
                    @endif

                    <table class="table" id="districtTable">
                            <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>User Level</th>
                                        <th>Designation</th>
                                        <th>Division</th>
                                        <th>District</th>
                                        <th>Self District</th>
                                        <th>Action</th>    
                                    </tr>   
                                </thead>
        
                                @forelse($users as $user)
                                <tbody>
                                    <tr>
                                        <td>
                                            {{ $user->name }}
                                        </td>
                                        <td>{{ $user->user_role->role->name ?? '' }}</td>
                                        <td>{{ $user->designation->name ?? '' }}</td>
                                        <td>{{ $user->user_attached_division->division->name ?? '' }}</td>
                                        <td>{{ $user->user_attached_district->district->name ?? '' }}</td>
                                        <td>{{ $user->district->name ?? '' }}</td>
                                        <td>
                                            <ul class="list-unstyled list-inline">
                                                <li class="list-inline-item">
                                                    <a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-primary">View</a>
                                                </li>
        
                                                <li class="list-inline-item">
                                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-sm btn-success">Edit</a>
                                                </li>
        
                                                <li class="list-inline-item">
                                                    <a href="#" class="btn btn-sm btn-danger" onclick="formSubmit('{{ $user->id }}')">Delete</a>
        
                                                    <form action="{{route('users.destroy', $user->id)}}" id="deleteForm_{{$user->id}}" method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                    </form>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </tbody>
        
                                @empty
                                    <tr>
                                        <td>
                                            No data found!
                                        </td>
                                    </tr>
                                @endforelse
                    </table>  

                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript" src="{{ asset('assets/js/plugins/sweetalert.min.js')}}"></script>



<script>

    
    function formSubmit(id)
        {
            
          swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data !",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel plz!",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function(isConfirm) {
            if (isConfirm) {
              $('#deleteForm_'+id).submit();
              swal("Deleted!", "Your data has been deleted.", "success");
            } else {
              swal("Cancelled", "Your data is safe :)", "error");
            }
          });
        }


</script>



@endsection
