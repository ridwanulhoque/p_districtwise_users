<?php

use Illuminate\Database\Seeder;
use App\Division;
use App\User;

class DivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $divisions = [
            'Dhaka',
            'Chittagong',
            'Rajshahi',
            'Khulna',
            'Barisal',
            'Rangpur',
            'Sylhet',
            'Mymensingh'
        ];

        foreach($divisions as $division){
            Division::create([
                'name' => $division
            ]);
        }

    }
}
