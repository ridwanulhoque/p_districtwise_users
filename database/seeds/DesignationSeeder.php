<?php

use Illuminate\Database\Seeder;
use App\Designation;

class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $designations = ['Project Manager', 'Programmer'];

        foreach($designations as $designation){
            Designation::create([
                'name' => $designation
            ]);
        }
    }
}
