<?php

use Illuminate\Database\Seeder;
use App\UserAttachedDistrict;
use App\User;

class UserAttachedDistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $district_users = User::whereHas('user_role', function($user_roles){
            $user_roles->where('role_id', 3);
        })->take(64)->pluck('id');

        foreach($district_users as $key => $district_user){
            UserAttachedDistrict::create([
                'user_id' => $district_user,
                'district_id' => $key + 1
            ]);
        }
    }
}
