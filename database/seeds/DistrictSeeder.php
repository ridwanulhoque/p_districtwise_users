<?php

use Illuminate\Database\Seeder;
use App\District;
use Illuminate\Foundation\Auth\User;

class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $districts = [
            [1, 'Narsingdi'],
            [1, 'Gazipur'],
            [1, 'Shariatpur'],
            [1, 'Narayanganj'],
            [1, 'Tangail'],
            [1, 'Kishoreganj'],
            [1, 'Manikganj'],
            [1, 'Dhaka'],
            [1, 'Munshiganj'],
            [1, 'Rajbari'],
            [1, 'Madaripur'],
            [1, 'Gopalganj'],
            [1, 'Faridpur'],
            [2, 'Comilla'],
            [2, 'Feni'],
            [2, 'Brahmanbaria'], 
            [2, 'Rangamati'],
            [2, 'Noakhali'],
            [2, 'Chandpur'],
            [2, 'Lakshmipur'],
            [2, 'Chittagong'],
            [2, 'Coxsbazar'],
            [2, 'Khagrachhari'],
            [2, 'Bandarban'],
            [3, 'Sirajganj'],
            [3, 'Pabna'],
            [3, 'Bogra'],
            [3, 'Rajshahi'],
            [3, 'Natore'],
            [3, 'Joypurhat'],
            [3, 'Chapainawabganj'],
            [3, 'Naogaon'],
            [4, 'Jessore'],
            [4, 'Satkhira'],
            [4, 'Meherpur'],
            [4, 'Narail'],
            [4, 'Chuadanga'],
            [4, 'Kushtia'],
            [4, 'Magura'],
            [4, 'Khulna'],
            [4, 'Bagerhat'],
            [4, 'Jhenaidah'],
            [5, 'Jhalakathi'],
            [5, 'Patuakhali'],
            [5, 'Pirojpur'],
            [5, 'Barisal'],
            [5, 'Bhola'],
            [5, 'Barguna'],
            [6, 'Panchagarh'],
            [6, 'Dinajpur'],
            [6, 'Lalmonirhat'],
            [6, 'Nilphamari'],
            [6, 'Gaibandha'],
            [6, 'Thakurgaon'],
            [6, 'Rangpur'],
            [6, 'Kurigram'],
            [7, 'Sylhet'],
            [7, 'Moulvibazar'],
            [7, 'Habiganj'],
            [7, 'Sunamganj'],
            [8, 'Sherpur'],
            [8, 'Mymensingh'],
            [8, 'Jamalpur'],
            [8, 'Netrokona'],
        ];

        foreach($districts as $key => $district){
            District::create([
                'name' => $district[1],
                'division_id' => $district[0]
            ]);
        }

    }
}
