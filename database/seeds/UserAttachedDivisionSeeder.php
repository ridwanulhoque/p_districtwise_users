<?php

use Illuminate\Database\Seeder;
use App\UserAttachedDivision;
use App\User;

class UserAttachedDivisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $division_users = User::whereHas('user_role', function($user_roles){
            $user_roles->where('role_id', 2);
        })->take(8)->pluck('id');

        foreach($division_users as $key => $division_user){
            UserAttachedDivision::create([
                'user_id' => $division_user,
                'division_id' => $key + 1
            ]);
        }
    }
}
