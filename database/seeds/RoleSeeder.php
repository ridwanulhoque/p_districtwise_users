<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
use App\UserRole;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['Admin', 'Division', 'District'];

        foreach($roles as $role){
            Role::create([
                'name' => $role,
                'slug' => strtolower($role)
            ]);
        }


        $user_ids = User::pluck('id');

        foreach($user_ids as $key => $user_id){
            //we need at least 8 division user and at least 64 district user
            $role_id = $key<=7 ? 2:($key<=71 ? 3: rand(1,3));
            
            UserRole::create([
                'user_id' => $user_id,
                'role_id' => $role_id
            ]);
        }

        

    }
}
